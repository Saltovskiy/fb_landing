(function ($) {
    $.validator.addMethod("characters", function (value, element) {
        return /^[а-яА-ЯЁёa-zA-Z\s]+$/.test(value);
    });

    $.validator.addMethod("phoneVal", function (value, element) {
        return /^[\d\(\)\s\-\+]{19}$/.test(value);
    });

    var overlay = $('.overlay');


    $(function () {
        initFormCallback('#callback-form');
    });

    function initFormCallback(formId) {
        $(formId).validate({
            onkeyup: false,
            onclick: false,
            rules: {
                firstName: {
                    required: true,
                    minlength: 2,
                    characters: true
                },
                phoneNum: {
                    required: true,
                    phoneVal: true
                }
            },
            messages: {
                firstName: {
                    required: "Заполните поле!",
                    minlength: "Введите минимум 2 символа!",
                    characters: "Имя может содержать только буквы!"
                },
                phoneNum: {
                    required: "Неверно заполнено поле!",
                    phoneVal: "Неверно заполнено поле!"
                }
            },

            highlight: function (element) {
                $(element).removeClass('valid');
                $(element).addClass('invalid');
            },
            unhighlight: function (element) {
                $(element).removeClass('invalid');
                $(element).addClass('valid');
            },
            submitHandler: function (form) {
                $(form).ajaxSubmit({
                    'type': 'POST',
                    'data': {},
                    'url': 'index.php?route=account/edit/ajax_edit',
                    beforeSend: function () {
                        $(formId).find("button[type='submit']").prop("disabled", true);
                    },
                    complete: function () {
                        $(formId).find("button[type='submit']").prop("disabled", false);
                    },
                    success: function (result, statusText, xhr, form) {
                        $(formId).parent('.inner').hide();
                        $(formId).parent('.inner').next('.inner-hide').show();
                        $(formId).get(0).reset();
                        setTimeout(function () {
                            $(formId).parent('.popup').removeClass('active');
                            overlay.removeClass('active');
                            $(formId).parent('.inner').show();
                            $(formId).parent('.inner').next('.inner-hide').hide();
                        }, 5000);

                    },
                    error: function (err) {

                    }
                });
            }

        });
    }


    $('input').on('keypress', function (event) {
        if (event.keyCode === 60 || event.keyCode === 62) {
            return false;
        }
    });

    $('input[type="tel"]').mask("+38 (999) 99-99-999");

})(jQuery);

